# HazemeyerIocaste PS Device Server

## Releases

* 2.0.2: Solve bugs found. See commit 80fcb18d.
* 2.0.1: Solve Bug with MAC address.
* 2.0.0: First version ready to work with python3.
* 1.30.2: First Debian verison of the Device that seems work as expected.
* 1.29.0: Last bliss version